<?php

function plis_products_loader($nid = NULL) {
    
    $query = new EntityFieldQuery();
    
    if($nid != NULL){
       $result = $query->entityCondition('entity_type', 'node')
            ->entityCondition('entity_id', $nid)
            ->entityCondition('bundle', array_keys(commerce_product_types()))
            ->execute();
    }
    else{
    $result = $query->entityCondition('entity_type', 'node')
            ->entityCondition('bundle', array_keys(commerce_product_types()))
            ->execute();
    }

    $type = 'node';
    $allnodes = array();
    $products = array();

    if (isset($result['node'])) {
        foreach ($result['node'] as $node) {
            $wrapper = entity_metadata_wrapper($type, $node->nid);
            $allnodes[] = $wrapper;
        }

        foreach ($allnodes as $onenode) {
            $commerce_product = $onenode->field_product->value();
            $body = $onenode->body->value();
            $body = $body['value'];

            if (property_exists($commerce_product[0], 'field_images')) {
                $images = $commerce_product[0]->field_images;
            }
            else {
                $images = array();
            }

            if (count($images) == 0) {
                $images = array();
            }
            else {
                $images = $commerce_product[0]->field_images['und'];
            }
            
             $vote_result = array();
            //reviews
            if (module_exists('votingapi')) {

                $votingapi_results = votingapi_select_results(array(
                  'entity_type' => 'node',
                  'entity_id' => $onenode->getIdentifier()
                ));

                $count = 0;
                $votingapi_functions = array('count', 'average');
                $vote_result = array();

                foreach ($votingapi_results as $result) {
                    if (!in_array($result['function'], $votingapi_functions)) { //custom options, need to tally these together to get a total count
                        $count += $result['value'];
                    }
                    else {
                        //$vote_result[0] = $result['entity_id'];
                        ${$result['function']} = $result['value'];

                        $vote_result[$result['function']] = $result['value'];

                    }
                }

            }

            $products[] = array(
                'nid' => $onenode->getIdentifier(),
                'product_id' => $commerce_product[0]->product_id,
                'body' => $body,
                'title_field' => $onenode->title_field->raw(),
                'commerce_price' => $commerce_product[0]->commerce_price['und'][0],
                'type' => $onenode->value()->type,
                'images' => $images,
                'vote' => $vote_result
            );
        }
    }

    return $products;
}
