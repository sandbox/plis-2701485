<?php

define('PLIS_TITLE_MIN_CHAR_LENGTH', 15);
define('PLIS_TITLE_MAX_CHAR_LENGTH', 50);
define('PLIS_TITLE_MIN_WORD_LENGTH', 3);
define('PLIS_TITLE_MAX_WORD_LENGTH', 15);

define('PLIS_DESC_MIN_CHAR_LENGTH', 100);
define('PLIS_DESC_MIN_WORD_LENGTH', 15);

define('PLIS_IMAGE_MIN_COUNT', 4);
define('PLIS_IMAGE_MIN_RES', 1000); //In KiloBytes

define('PLIS_RATE_AVG_PERCENTAGE', 50);


function plis_product_improvement_check($productList) {

  $improvements = array();
  foreach ($productList as $product) {
    //dsm($product);
    if (array_key_exists('title_field', $product)) {
      $improvements = plis_array_merge($improvements, plis_title_improvement_check($product));
      $improvements = plis_array_merge($improvements, plis_desc_improvement_check($product));
      $improvements = plis_array_merge($improvements, plis_image_improvement_check($product));
      $improvements = plis_array_merge($improvements, plis_price_improvement_check($product));
      $improvements = plis_array_merge($improvements, plis_reviews_improvement_check($product));
    }
  }
//  dsm($improvements);

  return $improvements;
}

function plis_title_improvement_check($product) {
//  dsm($product);
  $pid = $product['product_id'];
  $nid = $product['nid'];
  $title = $product['title_field'];
  $field = 'title';
  $fault = NULL;
  $improvements = array();
  if (strlen($title) < PLIS_TITLE_MIN_CHAR_LENGTH) {
    $fault = sprintf(t("Title is too short, consider adding more detail"));
    $improvements[$pid] = plis_create_deficiency($pid, $nid, $title, $field, $fault, -5);
  }
  elseif (strlen($title) > PLIS_TITLE_MAX_CHAR_LENGTH) {
    $fault = sprintf(t("Title is too long, consider being more concise"));
    $improvements[$pid] = plis_create_deficiency($pid, $nid, $title, $field, $fault, -5);
  }
  elseif (str_word_count($title) < PLIS_TITLE_MIN_WORD_LENGTH) {
    $fault = sprintf(t("Title is too short, consider adding more detail"));
    $improvements[$pid] = plis_create_deficiency($pid, $nid, $title, $field, $fault, 0);
  }
  elseif (str_word_count($title) > PLIS_TITLE_MAX_WORD_LENGTH) {
    $fault = sprintf(t("Title is too long, consider being more concise"));
    $improvements[$pid] = plis_create_deficiency($pid, $nid, $title, $field, $fault, 0);
  }
  return $improvements;
}

function plis_price_improvement_check($product) {
  $improvements = array();
  if (array_key_exists('commerce_price', $product)) {
    $pid = $product['product_id'];
    $nid = $product['nid'];
    $title = $product['title_field'];
    $price = $product['commerce_price']['amount'];
    $field = 'price';
    $fault = NULL;
    if ($price <= 0) {
      $fault = sprintf(t("Price is too low, consider increasing it"));
      $improvements[$pid] = plis_create_deficiency($pid, $nid, $title, $field, $fault, -5);
    }
  }
  return $improvements;
}

function plis_desc_improvement_check($product) {
  $improvements = array();
  if (array_key_exists('body', $product)) {
    $pid = $product['product_id'];
    $nid = $product['nid'];
    $title = $product['title_field'];
    $desc = $product['body'];
    $field = 'description';
    $fault = NULL;
    if (strlen($desc) < PLIS_DESC_MIN_CHAR_LENGTH) {
      $fault = sprintf(t("Description is too short, consider adding more detail"));
      $improvements[$pid] = plis_create_deficiency($pid, $nid, $title, $field, $fault, -4);
    }
    elseif (str_word_count($desc) < PLIS_DESC_MIN_WORD_LENGTH) {
      $fault = sprintf(t("Description is too short, consider adding more detail"));
      $improvements[$pid] = plis_create_deficiency($pid, $nid, $title, $field, $fault, 0);
    }
  }
  return $improvements;
}



function plis_image_improvement_check($product)
{
    $improvements = array();
    if (array_key_exists('images', $product)) {
        $pid = $product['product_id'];
        $nid = $product['nid'];
        $title = $product['title_field'];
        $field = 'Image';
        $image_array = $product['images'];
        $fault = NULL;

        if (count($image_array) == 0) {
            $fault = sprintf(t("Product does not have any images, consider adding some images to increase interest"));
            $improvements[$pid] = plis_create_deficiency($pid, $nid, $title, $field, $fault, -4);
        } elseif (count($image_array) < PLIS_IMAGE_MIN_COUNT) {
            $fault = sprintf(t("Product only has %d images, consider adding more images to increase interest"),
                count($image_array));
            $improvements[$pid] = plis_create_deficiency($pid, $nid, $title, $field, $fault, -4);
        }
        foreach ($image_array as $image) {
            if ($image['height'] * $image['width'] < PLIS_IMAGE_MIN_RES * 1024) {
                $fault = sprintf(t("Image %s resolution is too low"), $image['filename']);
                $improvements[$pid] = plis_create_deficiency($pid, $nid, $title, $field, $fault, -5);
            }
            //  dsm($improvements);
        }

    }
    return $improvements;
}

function plis_reviews_improvement_check($product) {
  $improvements = array();
    if (count($product['vote']) > 0) {
      $pid = $product['product_id'];
      $nid = $product['nid'];
      $title = $product['title_field'];
      $vote = $product['vote'];
      $field = 'vote';
      $fault = NULL;

      if ($vote == NULL) {
        $fault = sprintf(t("Product has no ratings, consider adding more detail"));
        $improvements[$pid] = plis_create_deficiency($pid, $nid, $title, $field, $fault, -4);
      }
      elseif ($vote['average'] < PLIS_RATE_AVG_PERCENTAGE) {
        $fault = sprintf(t("Product rating is lower than %d%% , consider adding more detail"), PLIS_RATE_AVG_PERCENTAGE);
        $improvements[$pid] = plis_create_deficiency($pid, $nid, $title, $field, $fault, -3);
      }
    }

  return $improvements;
}

function plis_array_merge($improvementList, $newImprovements){
    if (count($improvementList) == 0){
        return $newImprovements;
    } else {
        foreach ($newImprovements as $improvement){
        if (array_key_exists($improvement['product_id'], $improvementList)){
            foreach ($improvement['improvements'] as $fault) {
                $improvementList[$improvement['product_id']]['improvements'][] = $fault;
                $improvementList[$improvement['product_id']]['weight'] += $fault['weight'];
            }
            } else {
                $improvementList[$improvement['product_id']] = $improvement;
            }
        }
    }
    return $improvementList;
}


function plis_create_deficiency($pid, $nid, $title, $field, $fault, $weight) {
  return array(
    'product_id' => $pid,
    'node_id' => $nid,
    'title' => $title,
    'weight' => $weight,
    'improvements' => array (
        array(
            'field' => $field,
            'fault' => $fault,
            'weight' => $weight
        )
    )
  );
}
