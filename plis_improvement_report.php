<?php
/**
 * Created by PhpStorm.
 * User: James Ladell
 * Date: 07/03/16
 * Time: 3:34 PM
 */
function plis_improvement_report($improvements)
{

    $header = array(
       array('data' => t('Product')),
       array('data' => t('Improvements')),
       array('data' => t('Score')),
    );

    usort($improvements, function($a, $b) {
        return $a['weight'] - $b['weight'];
    });

    $rows = array();
    foreach ($improvements as $row) {
        usort($row['improvements'], function($a, $b) {
            return $a['weight'] - $b['weight'];
        });
        $fault = '<ul>';
        foreach ($row['improvements'] as $improvement){
            $fault .= '<li>' . $improvement['fault'] . ' (' . $improvement['weight'] . ')</li>';
        }
        $fault .= '</ul>';
        $rows[] = array(l($row['title'], 'node/' . $row['node_id']),
            $fault,
            $row['weight']);
    }
    $output = theme('table', array('header' => $header, 'rows' => $rows));
    return $output;
}
