<?php

function plis_show_page($nid = NULL) {
    global $improvements;
    include_once('plis_products_info_builder.php');
    include_once('plis_improvement_check.inc');
    include_once('plis_improvement_report.php');
       
    $productList = plis_products_loader($nid);
    $improvements = plis_product_improvement_check($productList);
    $report = plis_improvement_report($improvements);

    return $report;
}





